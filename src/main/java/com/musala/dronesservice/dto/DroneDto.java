package com.musala.dronesservice.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.musala.dronesservice.exception.BadRequestException;
import com.musala.dronesservice.model.DroneModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class DroneDto {
    private Integer id;
    private String serialNumber;
    private Double weightLimit;
    private String model;
    private Double batteryCapacity;
    private String state;
    private List<MedicationDto> medications;

    public DroneDto(String serialNumber, Double batteryCapacity) {
        this.serialNumber = serialNumber;
        this.batteryCapacity = batteryCapacity;
    }

    public void isValidRegistration() {
        if (this.serialNumber == null || this.serialNumber.length() > 100) {
            throw new BadRequestException("Serial cannot be empty or greater than 100 characters");
        }

        if (this.weightLimit == null || this.weightLimit > 500) {
            throw new BadRequestException("Weight limit is mandatory and must not be greater than 500g");
        }

        if (DroneModel.fromName(this.model) == null) {
            throw new BadRequestException("The provided drone model is invalid");
        }
    }

    public void isValidLoading() {
        if (this.serialNumber == null) {
            throw new BadRequestException("Serial number is a mandatory");
        }
        if (CollectionUtils.isEmpty(this.medications)) {
            throw new BadRequestException("At least 1 medication is required to load a drone");
        }

    }
}
