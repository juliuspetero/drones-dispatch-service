package com.musala.dronesservice.config;

import com.musala.dronesservice.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

@Component
public class ScheduledTaskConfig {

    private static final Logger LOG = Logger.getLogger(ScheduledTaskConfig.class.getName());
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private final DroneService droneService;

    @Autowired
    public ScheduledTaskConfig(DroneService droneService) {
        this.droneService = droneService;
    }

    @Scheduled(fixedRate = 20000)
    public void scheduleBatteryLevelCheckTaskWithFixedRate() {
        LOG.info("Fixed Rate Task :: Start Execution Time - " + dateTimeFormatter.format(LocalDateTime.now()));
        droneService.runPeriodicBatteryCheck();
        LOG.info("Fixed Rate Task :: End Execution Time - " + dateTimeFormatter.format(LocalDateTime.now()));
    }
}
