package com.musala.dronesservice.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventType {
    BATTERY_LEVEL_CHECK(1, "BatteryLevelCheck");

    private final Integer index;
    private final String name;
}
