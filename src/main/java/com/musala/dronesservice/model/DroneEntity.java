package com.musala.dronesservice.model;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Setter
@Entity(name = "drone")
@NoArgsConstructor
@AllArgsConstructor
public class DroneEntity extends BaseEntity {
    private String serialNumber;
    private Double weightLimit;
    private DroneModel model;
    private Double batterCapacity;
    private DroneState state;
    private List<MedicationEntity> medications;

    @Column(name = "serial_number")
    public String getSerialNumber() {
        return serialNumber;
    }

    @Column(name = "weight_limit")
    public Double getWeightLimit() {
        return weightLimit;
    }

    @Column(name = "model")
    @Enumerated(EnumType.STRING)
    public DroneModel getModel() {
        return model;
    }

    @Column(name = "battery_capacity")
    public Double getBatterCapacity() {
        return batterCapacity;
    }

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    public DroneState getState() {
        return state;
    }
    @JoinTable(name = "drone_medication", joinColumns = {
            @JoinColumn(name = "drone_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "medication_id", referencedColumnName = "id")})
    @OneToMany(targetEntity = MedicationEntity.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<MedicationEntity> getMedications() {
        return medications;
    }
}
