package com.musala.dronesservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum DroneModel {
    LIGHT_WEIGHT(1, "Lightweight"),
    MIDDLE_WEIGHT(2, "Middleweight"),
    CRUISER_WEIGHT(3, "Cruiserweight"),
    HEAVY_WEIGHT(4, "Heavyweight");

    private final Integer index;
    private final String name;

    public static DroneModel fromName(String name) {
        return Stream.of(values())
                .filter(m -> m.getName().equals(name))
                .findFirst()
                .orElse(null);
    }
}
