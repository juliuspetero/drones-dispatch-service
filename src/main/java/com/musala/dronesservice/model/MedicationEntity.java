package com.musala.dronesservice.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Setter
@Entity(name = "medication")
@NoArgsConstructor
@AllArgsConstructor
public class MedicationEntity extends BaseEntity {

    private String name;
    private Double weight;
    private String code;
    private String imageName;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "weight")
    public Double getWeight() {
        return weight;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    @Column(name = "image_name")
    public String getImageName() {
        return imageName;
    }
}
