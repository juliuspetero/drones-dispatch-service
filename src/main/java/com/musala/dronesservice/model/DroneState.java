package com.musala.dronesservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DroneState {
    IDLE(1, "IDLE"),
    LOADING(2, "LOADING"),
    LOADED(3, "LOADED"),
    DELIVERING(4, "DELIVERING"),
    DELIVERED(5, "DELIVERED"),
    RETURNING(6, "RETURNING");

    private final Integer index;
    private final String name;
}
