package com.musala.dronesservice.model;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Setter
@Entity(name = "event_log")
@NoArgsConstructor
@AllArgsConstructor
public class EventLogEntity extends BaseEntity {
    private EventType eventType;
    private String body;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public EventType getEventType() {
        return eventType;
    }

    @Column(name = "body")
    public String getBody() {
        return body;
    }
}
