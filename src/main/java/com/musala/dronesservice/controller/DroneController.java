package com.musala.dronesservice.controller;

import com.musala.dronesservice.dto.ActionDto;
import com.musala.dronesservice.dto.DroneDto;
import com.musala.dronesservice.dto.MedicationDto;
import com.musala.dronesservice.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/drone")
public class DroneController {
    private final DroneService droneService;

    @Autowired
    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @RequestMapping(path="/register", method = RequestMethod.POST)
    public DroneDto register(@RequestBody DroneDto droneDto){
        return droneService.register(droneDto);
    }

    @RequestMapping(path="/load", method = RequestMethod.POST)
    public ActionDto load(@RequestBody DroneDto droneDto){
         droneDto = droneService.load(droneDto);
         return new ActionDto(droneDto.getSerialNumber(), "Drone successfully loaded");
    }

    @RequestMapping(path="/droneBySerialNumber/{serialNumber}", method = RequestMethod.GET)
    public DroneDto droneBySerialNumber(@PathVariable String serialNumber){
        return droneService.fetchDroneBySN(serialNumber);
    }

    @RequestMapping(path="/medications/{serialNumber}", method = RequestMethod.GET)
    public List<MedicationDto> fetchMedications(@PathVariable String serialNumber){
        return droneService.fetchMedications(serialNumber);
    }

    @RequestMapping(path="/availableDrones", method = RequestMethod.GET)
    public List<DroneDto> availableDrones(){
        return droneService.fetchAvailableDrones();
    }

}
