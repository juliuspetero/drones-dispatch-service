package com.musala.dronesservice.repository;

import com.musala.dronesservice.model.DroneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneJpaRepository extends JpaRepository<DroneEntity, Integer> {

    Optional<DroneEntity> findBySerialNumber(String serialNumber);

    @Query(value = "SELECT * FROM drone d INNER JOIN drone_medication dm ON dm.drone_id = d.id WHERE dm.medication_id IS NOT NULL AND d.state IN (:states) AND d.battery_capacity > 25.0", nativeQuery = true)
    List<DroneEntity> findAvailableDrones(List<String> states);

}
