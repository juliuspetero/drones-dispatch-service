package com.musala.dronesservice.repository;

import com.musala.dronesservice.model.MedicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationJpaRepository extends JpaRepository<MedicationEntity, Integer> {

    @Query(value = "SELECT * FROM medication m WHERE m.code IN :codes", nativeQuery = true)
    List<MedicationEntity> findByAllByCode(List<String> codes);

    @Override
    List<MedicationEntity> findAllById(Iterable<Integer> integers);
}
