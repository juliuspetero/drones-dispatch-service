package com.musala.dronesservice.repository;

import com.musala.dronesservice.model.EventLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventLogJpaRepository extends JpaRepository<EventLogEntity, Integer> {
}
