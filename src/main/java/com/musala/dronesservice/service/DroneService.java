package com.musala.dronesservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.dronesservice.dto.DroneDto;
import com.musala.dronesservice.dto.MedicationDto;
import com.musala.dronesservice.exception.BadRequestException;
import com.musala.dronesservice.model.*;
import com.musala.dronesservice.repository.DroneJpaRepository;
import com.musala.dronesservice.repository.EventLogJpaRepository;
import com.musala.dronesservice.repository.MedicationJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class DroneService {

    private static final Logger LOG = Logger.getLogger(DroneService.class.getName());

    private final DroneJpaRepository droneJpaRepository;
    private final MedicationJpaRepository medicationJpaRepository;
    private final EventLogJpaRepository eventLogJpaRepository;

    @Autowired
    public DroneService(DroneJpaRepository droneJpaRepository, MedicationJpaRepository medicationJpaRepository, EventLogJpaRepository eventLogJpaRepository) {
        this.droneJpaRepository = droneJpaRepository;
        this.medicationJpaRepository = medicationJpaRepository;
        this.eventLogJpaRepository = eventLogJpaRepository;
    }

    @Transactional
    public DroneDto register(DroneDto droneDto) {
        droneDto.isValidRegistration();
        Optional<DroneEntity> droneEntityOptional = this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber());
        if (droneEntityOptional.isPresent()) {
            throw new BadRequestException("Drone with the provided serial number is already registered");
        }
        DroneEntity droneEntity = new DroneEntity();
        droneEntity.setSerialNumber(droneDto.getSerialNumber());
        droneEntity.setWeightLimit(droneDto.getWeightLimit());
        droneEntity.setModel(DroneModel.fromName(droneDto.getModel()));
        droneEntity.setState(DroneState.IDLE);
        droneEntity.setBatterCapacity(100.00d);
        this.droneJpaRepository.save(droneEntity);
        droneDto.setId(droneEntity.getId());
        return droneDto;
    }

    @Transactional
    public DroneDto load(DroneDto droneDto) {
        droneDto.isValidLoading();
        DroneEntity droneEntity = this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber()).orElseThrow(() -> new BadRequestException("Drone with provided serial does not exist here!"));
        if (!CollectionUtils.isEmpty(droneEntity.getMedications())) {
            throw new BadRequestException("Drone has already been loaded!");
        }
        List<MedicationEntity> medications = this.medicationJpaRepository.findByAllByCode(droneDto.getMedications().stream().map(MedicationDto::getCode).collect(Collectors.toList()));
        if (CollectionUtils.isEmpty(medications)) {
            throw new BadRequestException("No medication found with the provided codes");
        }
        Double totalWeight = medications.stream().map(MedicationEntity::getWeight).reduce(0d, Double::sum);
        if (totalWeight > droneEntity.getWeightLimit()) {
            throw new BadRequestException("Medication loaded has exceeded the maximum weight of " + droneEntity.getWeightLimit());
        }
        if (droneEntity.getBatterCapacity() < 25.0) {
            throw new BadRequestException("The battery level is too low for loading!");
        }
        droneEntity.setMedications(medications);
        droneEntity.setState(DroneState.LOADED);
        this.droneJpaRepository.save(droneEntity);
        return droneDto;
    }

    public List<MedicationDto> fetchMedications(String serialNumber) {
        DroneEntity droneEntity = this.droneJpaRepository.findBySerialNumber(serialNumber).orElseThrow(() -> new BadRequestException("Drone with provided serial does not exist here!"));
        List<MedicationEntity> medicationEntities = droneEntity.getMedications();
        return medicationEntities.stream().map(this::toDto).collect(Collectors.toList());
    }

    private MedicationDto toDto(MedicationEntity medicationEntity) {
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setCode(medicationEntity.getCode());
        medicationDto.setName(medicationEntity.getName());
        medicationDto.setImageName(medicationEntity.getImageName());
        medicationDto.setWeight(medicationEntity.getWeight());
        return medicationDto;
    }

    public List<DroneDto> fetchAvailableDrones() {
        List<DroneEntity> droneEntities = this.droneJpaRepository.findAvailableDrones(List.of(DroneState.IDLE.name()));
        return droneEntities.stream().map(this::toDto).collect(Collectors.toList());
    }

    public DroneDto fetchDroneBySN(String serialNumber) {
        DroneEntity droneEntity = this.droneJpaRepository.findBySerialNumber(serialNumber).orElseThrow(() -> new BadRequestException("Drone with provided serial does not exist here!"));
        return toDto(droneEntity);
    }

    private DroneDto toDto(DroneEntity droneEntity) {
        DroneDto droneDto = new DroneDto();
        droneDto.setBatteryCapacity(droneEntity.getBatterCapacity());
        droneDto.setSerialNumber(droneEntity.getSerialNumber());
        droneDto.setModel(droneEntity.getModel().getName());
        droneDto.setState(droneEntity.getState().getName());
        droneDto.setWeightLimit(droneEntity.getWeightLimit());
        droneDto.setId(droneEntity.getId());
        return droneDto;
    }

    public void runPeriodicBatteryCheck() {
        LOG.info("======== Started periodic task to check battery level ========");
        try {
            List<DroneDto> droneDtos = fetchDroneBatterLevels();
            String jsonStr = new ObjectMapper().writeValueAsString(droneDtos);
            this.eventLogJpaRepository.save(new EventLogEntity(EventType.BATTERY_LEVEL_CHECK, jsonStr));
            if (!CollectionUtils.isEmpty(droneDtos)) {
                for (DroneDto droneDto : droneDtos) {
                    DroneEntity droneEntity = this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber()).orElse(null);
                    if (droneEntity != null) {
                        droneEntity.setBatterCapacity(droneDto.getBatteryCapacity());
                        this.droneJpaRepository.save(droneEntity);
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "===== Error while running scheduled task =====", ex);
        }
    }

    // External service call to fetch drone batter levels
    private List<DroneDto> fetchDroneBatterLevels() {
        List<DroneDto> arr = new ArrayList<>();
        arr.add(new DroneDto("Ultra HD 4K Camera Drone", 10.00));
        arr.add(new DroneDto("Wipkviey T27 Foldable Drone", 35.00));
        arr.add(new DroneDto("DJI Tello Ryze", 35.00));
        return arr;
    }
}
