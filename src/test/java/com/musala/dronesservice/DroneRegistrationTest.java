package com.musala.dronesservice;

import com.musala.dronesservice.dto.DroneDto;
import com.musala.dronesservice.exception.BadRequestException;
import com.musala.dronesservice.model.DroneEntity;
import com.musala.dronesservice.model.DroneModel;
import com.musala.dronesservice.repository.DroneJpaRepository;
import com.musala.dronesservice.service.DroneService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(properties = {"spring.liquibase.enabled=false"})
@AutoConfigureMockMvc
public class DroneRegistrationTest {

    @InjectMocks
    private DroneService droneService;

    @Mock
    private DroneJpaRepository droneJpaRepository;


    @Test
    public void registerDroneWithEmptySerialNumber_shouldFail() {
        DroneDto droneDto = new DroneDto(null, 78.99);
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.register(droneDto));
        String expectedMessage = "Serial cannot be empty or greater than 100 characters";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void registerDroneWithWeightLimitGreaterThan500_shouldFail() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 1000000.99);
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.register(droneDto));
        String expectedMessage = "Weight limit is mandatory and must not be greater than 500g";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }


    @Test
    public void registerDroneWithSimilarName_shouldResultsIntoError() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 78.99);
        droneDto.setModel(DroneModel.MIDDLE_WEIGHT.getName());
        droneDto.setWeightLimit(500.00);
        Mockito.when(this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber())).thenReturn(Optional.of(new DroneEntity()));
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.register(droneDto));
        String expectedMessage = "Drone with the provided serial number is already registered";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
