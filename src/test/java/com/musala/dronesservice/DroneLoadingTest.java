package com.musala.dronesservice;


import com.musala.dronesservice.dto.DroneDto;
import com.musala.dronesservice.dto.MedicationDto;
import com.musala.dronesservice.exception.BadRequestException;
import com.musala.dronesservice.model.DroneEntity;
import com.musala.dronesservice.model.MedicationEntity;
import com.musala.dronesservice.repository.DroneJpaRepository;
import com.musala.dronesservice.repository.MedicationJpaRepository;
import com.musala.dronesservice.service.DroneService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;

@SpringBootTest(properties = {"spring.liquibase.enabled=false"})
@AutoConfigureMockMvc
public class DroneLoadingTest {

    @InjectMocks
    private DroneService droneService;

    @Mock
    private DroneJpaRepository droneJpaRepository;

    @Mock
    private MedicationJpaRepository medicationJpaRepository;

    @Test
    public void loadingDroneWithEmptySerialNumber_mustFails() {
        DroneDto droneDto = new DroneDto(null, 78.99);
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.load(droneDto));
        String expectedMessage = "Serial number is a mandatory";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }


    @Test
    public void cannotLoadUnRegisteredDroneTest() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 78.99);
        droneDto.setMedications(List.of(new MedicationDto()));
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.load(droneDto));
        String expectedMessage = "Drone with provided serial does not exist here!";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void droneCanBeLoadedWithKnownMedicationTest() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 78.99);
        droneDto.setMedications(List.of(new MedicationDto()));
        Mockito.when(this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber())).thenReturn(Optional.of(new DroneEntity()));
        Mockito.when(this.medicationJpaRepository.findByAllByCode(anyList())).thenReturn(Collections.emptyList());
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.load(droneDto));
        String expectedMessage = "No medication found with the provided codes";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void droneCannotBeOverloadedWithWeight() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 78.99);
        droneDto.setMedications(List.of(new MedicationDto()));
        Mockito.when(this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber())).thenReturn(Optional.of(new DroneEntity() {{
            setWeightLimit(500.00);
        }}));
        Mockito.when(this.medicationJpaRepository.findByAllByCode(anyList())).thenReturn(List.of(new MedicationEntity() {{
            setWeight(1000.00);
        }}));
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.load(droneDto));
        String expectedMessage = "Medication loaded has exceeded the maximum weight of 500.0";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }


    @Test
    public void droneCannotBeLoadedWhenTheBattery_LessThan25Percent() {
        DroneDto droneDto = new DroneDto("Wipkviey T27 Foldable Drone", 78.99);
        droneDto.setMedications(List.of(new MedicationDto()));
        Mockito.when(this.droneJpaRepository.findBySerialNumber(droneDto.getSerialNumber())).thenReturn(Optional.of(new DroneEntity() {{
            setWeightLimit(500.00);
            setBatterCapacity(10.00);
        }}));
        Mockito.when(this.medicationJpaRepository.findByAllByCode(anyList())).thenReturn(List.of(new MedicationEntity() {{
            setWeight(190.00);
        }}));
        Exception exception = assertThrows(BadRequestException.class, () -> droneService.load(droneDto));
        String expectedMessage = "The battery level is too low for loading!";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
