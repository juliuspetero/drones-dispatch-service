# Drones-Dispatch-Service

## Description
A SpingBoot RESTful application to communicate with drones

## Pre-requistes
- Ensure that Docker is installed and running in your computer
- Ensure that Maven, the dependency manager is also installed and set the environment path
- Ensure that the project is fully clone to your computer

## Build
- Navigate to the root of the project 
- Start MySQL8 using command `docker-compose up`
- Build and run the project using `mvn spring-boot:run`
- You can as well package the application in jar file run it using `mvn package` & `java -jar ./target/Drones-Dispatch-Service-0.0.1-SNAPSHOT.jar` 

## Test
`mvn clean test`

## Endpoints
- Register a new drone
```aidl
curl --location --request POST 'http://localhost:9098/v1/drone/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serialNumber": "Fixed-Wing-ybrid-VTOL",
    "model": "Heavyweight",
    "weightLimit": 400,
    "batterCapacity": 100.00,
    "state": "IDLE"
}'
```
- Load a drone with medication items
```aidl
curl --location --request POST 'http://localhost:9098/v1/drone/load' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serialNumber": "Fixed-Wing-ybrid-VTOL",
    "medications": [
        {
            "code": "CIPRO_18928_MEDIC"
        },
        {
            "code": "CET_HYDRO_999001"
        }
    ]
}'
```

- Check loaded medication items for a given drone
```aidl
curl --location --request GET 'http://localhost:9098/v1/drone/medications/Fixed-Wing-ybrid-VTOL'
```

- Check available drones for loading
```aidl
curl --location --request GET 'http://localhost:9098/v1/drone/availableDrones'
```

- Check battery level for a given drone
```aidl
curl --location --request GET 'http://localhost:9098/v1/drone/droneBySerialNumber/Fixed-Wing-ybrid-VTOL'
```



